from django.contrib import admin
from animelist.models import UserScoreRequest, StateModel, DataBaseUpdate


class UserScoreRequestAdmin(admin.ModelAdmin):
    list_display = ('username', 'full_len', 'last_success_request', 'error')


class StateModelAdmin(admin.ModelAdmin):
    list_display = ('name', 'date')


admin.site.register(UserScoreRequest, UserScoreRequestAdmin)
admin.site.register(StateModel, StateModelAdmin)

admin.site.register(DataBaseUpdate)