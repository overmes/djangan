# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'UserScoreRequest'
        db.create_table(u'animelist_userscorerequest', (
            ('username', self.gf('django.db.models.fields.CharField')(max_length=200, primary_key=True)),
            ('user_id', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('full_len', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('last_success_request', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('error', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
        ))
        db.send_create_signal(u'animelist', ['UserScoreRequest'])

        # Adding model 'StateModel'
        db.create_table(u'animelist_statemodel', (
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100, primary_key=True)),
            ('date', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('fields', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'animelist', ['StateModel'])


    def backwards(self, orm):
        # Deleting model 'UserScoreRequest'
        db.delete_table(u'animelist_userscorerequest')

        # Deleting model 'StateModel'
        db.delete_table(u'animelist_statemodel')


    models = {
        u'animelist.statemodel': {
            'Meta': {'object_name': 'StateModel'},
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'fields': ('django.db.models.fields.TextField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'primary_key': 'True'})
        },
        u'animelist.userscorerequest': {
            'Meta': {'object_name': 'UserScoreRequest'},
            'error': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'full_len': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'last_success_request': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'user_id': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '200', 'primary_key': 'True'})
        }
    }

    complete_apps = ['animelist']