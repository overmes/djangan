# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'DataBaseUpdate'
        db.create_table(u'animelist_databaseupdate', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('date', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'animelist', ['DataBaseUpdate'])


    def backwards(self, orm):
        # Deleting model 'DataBaseUpdate'
        db.delete_table(u'animelist_databaseupdate')


    models = {
        u'animelist.databaseupdate': {
            'Meta': {'object_name': 'DataBaseUpdate'},
            'date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'animelist.statemodel': {
            'Meta': {'object_name': 'StateModel'},
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'fields': ('django.db.models.fields.TextField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'primary_key': 'True'})
        },
        u'animelist.userscorerequest': {
            'Meta': {'object_name': 'UserScoreRequest'},
            'error': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'full_len': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'last_success_request': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'user_id': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '200', 'primary_key': 'True'})
        }
    }

    complete_apps = ['animelist']