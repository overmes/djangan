from django.db import models
from django.utils.timezone import now

from title_loader.user_spider import UserSpider
from userspider.generators import OneUserTaskGenerator
from userspider.savers import AbstractSaver
import pymongo
from django.conf import settings
from libs.uniq_slug import unique_slugify


class UserSaver(AbstractSaver):
    def __init__(self, model):
        AbstractSaver.__init__(self)
        self.model = model

    def save_result(self, user_data):
        self.model.user_id = user_data.uid
        self.model.username = user_data.username
        self.model.full_len = user_data.anime_score_len + user_data.manga_score_len
        if not user_data.error:
            self.model.error = ''
            self.model.last_success_request = now()

        else:
            self.model.error = user_data.error
        scores = user_data.score_union()
        if scores:
            self.save_scores_in_mongo(user_data.uid, scores)

    @staticmethod
    def save_scores_in_mongo(user_id, scores):
        con = pymongo.MongoClient(host=settings.MONGODB_HOST, port=settings.MONGODB_PORT, w=0)
        db = con[settings.MONGODB_DATABASE]
        db.command("collMod", 'anime', usePowerOf2Sizes=True)
        col = db.anime
        for item_id, (score, type, date) in scores.items():
            item_id = int(item_id)
            if item_id > 0:
                item_id = {'i': item_id, 't': {'$in': ["TV", "Movie", "OVA", "Special", "ONA", "Music"]}}
            else:
                item_id = {'i': abs(item_id), 't': {'$in': ["Doujin", "Manhwa", "Manhua", "Novel", "One Shot", "Manga"]}}
            nested_doc = 'user_scores.' + str(user_id)
            col.update({'_id.i': item_id['i'], '_id.t': item_id['t']},
                       {'$set': {nested_doc: {'score': score, 'type': type, 'date': date}}})


class UserScoreRequest(models.Model):
    username = models.CharField(max_length=200, primary_key=True)
    user_id = models.IntegerField(null=True)
    full_len = models.IntegerField(null=True)
    last_success_request = models.DateTimeField(null=True)
    error = models.CharField(max_length=200, blank=True)

    def __unicode__(self):
        return self.username

    def update_scores(self):
        saver = UserSaver(model=self)
        generator = OneUserTaskGenerator(username=self.username)
        bot = UserSpider(generator=generator, saver=saver, thread_number=1,
                         task_try_limit=30, network_try_limit=30)
        bot.run()
        self.save()


class StateModel(models.Model):
    name = models.CharField(max_length=100, primary_key=True)
    date = models.DateTimeField(auto_now=True)
    fields = models.TextField()

    def save(self, **kwargs):
        unique_slugify(self, self.name, slug_field_name='name')
        super(StateModel, self).save(**kwargs)

    def __unicode__(self):
        return self.name


class DataBaseUpdate(models.Model):
    date = models.DateTimeField()

    def __unicode__(self):
        return str(self.date)
