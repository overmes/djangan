import json
import re
from tastypie import fields
from tastypie.cache import NoCache, SimpleCache
from animelist.models import DataBaseUpdate
from libs.mongodb_resource import MongoDBResource
from tastypie.resources import Resource, ModelResource
from django.core.cache import cache

name_cache = {}


class AnimeListResource(MongoDBResource):
    related = fields.CharField(readonly=True)

    def alter_list_data_to_serialize(self, request, data):
        data['meta']['total_count'] = self.totalCount
        return data

    def obj_get(self, request=None, **kwargs):
        pk = json.loads(kwargs.get("pk"))
        return self.get_collection().find_one({'_id': pk})

    def dehydrate_related(self, bundle):
        current_related = bundle.obj.get('related', {})
        for rel, array in current_related.items():
            for val in array:
                title = cache.get('{}{}'.format(val.get('i'), val.get('t')))
                if not title:
                    if val.get('t') == 'anime':
                        types = ["TV", "Movie", "OVA", "Special", "ONA", "Music"]
                    else:
                        types = ["Doujin", "Manhwa", "Manhua", "Novel", "One Shot", "Manga"]
                    title = (self.get_collection().find_one(
                        {'_id.i': val.get('i'), '_id.t': {'$in': types}},
                        fields={'title': 1}
                    ) or {}).get('title')
                    cache.set('{}{}'.format(val.get('i'), val.get('t')), title, 3600)
                val['title'] = title
        return current_related

    class Meta:
        allowed_methods = ['get']
        collection = 'anime'
        cache = NoCache()
        max_limit = 0


class TypeListResource(MongoDBResource):
    def get_object_list(self, request):
        query = request.GET.get('q', '')
        field = request.GET.get('field', '')

        regex = re.compile(u'.*{0}.*'.format(query), re.IGNORECASE)
        result = self.get_collection().aggregate([
            {'$unwind': '$' + field},
            {'$group': {
                '_id': '$' + field
            }},
            {'$match': {'_id': {'$regex': regex}}}
        ])

        return result['result']

    class Meta:
        allowed_methods = ['get']
        collection = 'anime'
        cache = SimpleCache(timeout=100)
        detail_allowed_methods = []
        list_allowed_methods = ['get']


class AnimeGroupListResource(MongoDBResource):
    @staticmethod
    def get_by_dot(x, name):
        if '.' in name:
            res = x
            paths = name.split('.')
            for p in paths:
                res = res.get(p) if res else None
        else:
            res = x.get(name)
        return res

    def get_matched_groups(self, filters, sort, sort_type):
        result = self.get_collection().aggregate([
            {'$match': filters},
            {
                '$group': {
                    '_id': '$group',
                    'res': {'$' + sort_type: '$' + sort[0][0]},
                    'ids': {'$addToSet': '$_id'}
                }
            },
            {'$sort': {'res': sort[0][1]}}
        ])
        return result['result']

    def get_matched_groups_count(self, filters):
        result = self.get_collection().aggregate([
            {'$match': filters},
            {
                '$group': {
                    '_id': 'id',
                    'groups': {'$addToSet': '$group'}
                }
            },
        ])
        return len(result['result'][0]['groups'])


    @staticmethod
    def get_anime_query(matched_groups_ids, filters):
        anime_query = dict()
        anime_query['spec'] = {'group': {'$in': matched_groups_ids}}
        anime_query['spec'].update(filters)
        return anime_query

    @staticmethod
    def get_group_query(matched_groups_ids, fields):
        anime_query = dict()
        anime_query['spec'] = {'group': {'$in': matched_groups_ids}}
        anime_query['fields'] = fields
        anime_query['fields'].append('group')
        return anime_query

    @staticmethod
    def get_sorted_anime_list(collection, matched_groups_ids, group_sort):
        def custom_cmp(x, y):
            res = cmp(matched_groups_ids.index(x.get('group')), matched_groups_ids.index(y.get('group')))
            if res == 0:
                res = group_sort[0][1] * cmp(AnimeGroupListResource.get_by_dot(x, group_sort[0][0]),
                                             AnimeGroupListResource.get_by_dot(y, group_sort[0][0]))
            return res

        return sorted(collection, cmp=custom_cmp)

    @staticmethod
    def get_top_n(collection, group_limit):
        def group_count(item, state=[0, 0]):
            item_group = item.get('group')
            if not item.get('filtered', False) and item_group != state[0]:
                state[0] = item_group
                state[1] = 1
            else:
                state[1] += 1
            if state[1] > group_limit:
                item['filtered'] = True
            return True

        return filter(group_count, collection)

    def get_object_list(self, request):
        sort_type = request.GET.get('sort_type', 'avg')

        limit = int(request.GET.get('query_limit', 100))
        group_limit = int(request.GET.get('group_limit', 100))
        skip = int(request.GET.get('skip', 0))

        filters = json.loads(request.GET.get('filters', '{}'))
        sort = json.loads(request.GET.get('sort', '[]'))
        group_sort = json.loads(request.GET.get('group_sort', '[]'))
        fields = json.loads(request.GET.get('fields', '[]'))

        matched_groups = self.get_matched_groups(filters, sort, sort_type)
        self.totalCount = len(matched_groups)
        matched_groups = matched_groups[skip:skip + limit]

        filtered_anime_ids = []
        map(lambda group: filtered_anime_ids.extend(group['ids']), matched_groups)
        matched_groups_ids = map(lambda group: group['_id'], matched_groups)

        group_query = self.get_group_query(matched_groups_ids, fields)
        full_groups = list(self.get_collection().find(**group_query))

        def set_filtered(anime):
            if anime['_id'] in filtered_anime_ids:
                anime['filtered'] = False
            else:
                anime['filtered'] = True

        map(set_filtered, full_groups)

        result_list = self.get_sorted_anime_list(full_groups, matched_groups_ids,
                                                 group_sort) if group_sort else full_groups
        result_list = self.get_top_n(result_list, group_limit) if group_limit < 100 else result_list

        return result_list

    def alter_list_data_to_serialize(self, request, data):
        data['meta']['total_count'] = self.totalCount
        return data

    class Meta:
        detail_allowed_methods = []
        list_allowed_methods = ['get']
        collection = 'anime'
        group_collection = 'groupres'
        cache = NoCache()


class HashCache(SimpleCache):

    def get(self, key, **kwargs):
        return self.cache.get(key.__hash__(), **kwargs)

    def set(self, key, value, timeout=None):
        if timeout is None:
            timeout = self.timeout
        self.cache.set(key.__hash__(), value, timeout)


class AnimeStatsResource(MongoDBResource):
    @staticmethod
    def get_group_aggregation():
        return {
            '$group': {
                '_id': "",
                'avg_duration': {'$avg': "$duration"},
                'sum_duration': {'$sum': "$duration"},
                'avg_episodes': {'$avg': "$episodes"},
                'sum_episodes': {'$sum': "$episodes"},
                'avg_members': {'$avg': "$members"},
                'sum_members': {'$sum': "$members"},
                'avg_scores': {'$avg': "$scores"},
                'sum_scores': {'$sum': "$scores"},
                'avg_score': {'$avg': "$avg"},
                'avg_weight': {'$avg': "$weight"},
                'count': {'$sum': 1},
                'group_array': {'$addToSet': "$group"},
            }
        }

    def obj_get(self, request=None, **kwargs):
        spec = json.loads(kwargs.get("pk"))
        query_stats = self.get_collection().aggregate([
            {'$match': spec},
            self.get_group_aggregation()
        ])

        res = query_stats[u'result'][0] if query_stats[u'result'] else {}
        if res:
            res['_id'] = kwargs.get("pk")

            res['group_count'] = len(res['group_array'])
            del res['group_array']

        return res

    class Meta:
        detail_allowed_methods = ['get']
        list_allowed_methods = []
        collection = 'anime'
        cache = HashCache(timeout=100)


class DatabaseUpdateResource(ModelResource):
    class Meta:
        queryset = DataBaseUpdate.objects.all().order_by('date')
        resource_name = 'dbupdate'
        ordering = ['date']