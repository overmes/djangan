from django.conf.urls import patterns, include, url
from tastypie.api import Api
from animelist.resources import AnimeListResource, AnimeStatsResource, AnimeGroupListResource, TypeListResource, \
    DatabaseUpdateResource

from animelist.views import IndexView, UserScoreRequestView, TestView, SaveStateView, JasmineView


v1_api = Api(api_name='v1')
v1_api.register(AnimeListResource())
v1_api.register(AnimeStatsResource())
v1_api.register(AnimeGroupListResource())
v1_api.register(TypeListResource())
v1_api.register(DatabaseUpdateResource())

urlpatterns = patterns('',
    url(r'^api/userscorerequest/$', UserScoreRequestView.as_view()),
    url(r'^api/userscorerequest/(?P<user_id>.*)/$', UserScoreRequestView.as_view()),
    url(r'^api/state/$', SaveStateView.as_view()),
    url(r'^api/state/(?P<state>.*)/$', SaveStateView.as_view()),
    url(r'^api/', include(v1_api.urls)),

    url(r'^$', IndexView.as_view(), name="index"),
    url(r'^jasmine/$', JasmineView.as_view(), name="jasmine"),
    url(r'^test/$', TestView.as_view()),
)