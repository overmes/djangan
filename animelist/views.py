import json
from braces.views import AjaxResponseMixin, JSONResponseMixin
from django.conf import settings
from django.forms import model_to_dict
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.generic.base import TemplateView, View
from animelist.models import UserScoreRequest, StateModel


class EnsureCsrfCookieMixin(object):

    @method_decorator(ensure_csrf_cookie)
    def dispatch(self, *args, **kwargs):
        return super(EnsureCsrfCookieMixin, self).dispatch(*args, **kwargs)


class IndexView(EnsureCsrfCookieMixin, TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['DEBUG'] = settings.DEBUG
        return context


class TestView(TemplateView):
    template_name = 'test.html'


class JasmineView(TemplateView):
    template_name = 'jasmine.html'


class UserScoreRequestView(JSONResponseMixin, AjaxResponseMixin, View):
    def post_ajax(self, request, *args, **kwargs):
        post_data = json.loads(request.body)
        user_score_request, created = UserScoreRequest.objects.get_or_create(username=post_data['username'])
        if created or user_score_request.error or not user_score_request.last_success_request or not user_score_request.user_id:
            user_score_request.update_scores()
        return self.render_json_response(model_to_dict(user_score_request))

    def put_ajax(self, request, *args, **kwargs):
        post_data = json.loads(request.body)
        user_score_request, created = UserScoreRequest.objects.get_or_create(username=post_data['username'])
        user_score_request.update_scores()
        return self.render_json_response(model_to_dict(user_score_request))


class SaveStateView(JSONResponseMixin, AjaxResponseMixin, View):
    def post_ajax(self, request, *args, **kwargs):
        post_data = json.loads(request.body)

        state = StateModel(name=post_data['name'], fields=json.dumps(post_data['fields']))
        state.save()

        post_data['id'] = state.name
        return self.render_json_response(post_data)

    def get(self, request, state):
        state_model = get_object_or_404(StateModel, pk=state)

        res = {"id": state_model.name, "fields": state_model.fields}
        return self.render_json_response(res)