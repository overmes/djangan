import argparse
import pymongo

arguments = argparse.ArgumentParser()
arguments.add_argument('-d', '--db', dest='db', type=str, default='test', help='mongodb for saving results')
args = arguments.parse_args()


con = pymongo.MongoClient()

anime_db = con[args.db]
anime_col = anime_db.anime

count = 1
for anime in anime_col.find():
    anime_col.update({'_id': anime['_id']}, {'$set': {'id': count}})
    count += 1