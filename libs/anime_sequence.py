import pymongo

con = pymongo.MongoClient()
db = con.anime_data
col = db.anime
group_col = db.anime_group

animes_sequence = {}

def date_diff(anime_date, related_date):
    return 1 if anime_date < related_date else -1

def get_diff(related_type, anime_date, related_date):
    type_diffs = {
                'sequel': 10,
                'prequel': -10,
                'alternative version': date_diff(anime_date, related_date),
                'parent story': -1,
                'adaptation': -1,
                'spin-off': 1,
                'other': 1,
                'side story': 1,
                'summary': 1,
                'alternative setting': date_diff(anime_date, related_date),
                'full story': -1,
                'character': 1,
            }
    return type_diffs[related_type]


def set_related(related, current_sequence, anime_date, open_set=set()):
    for related_type, related_list in related.items():
        for related_id in related_list:
            anime = col.find_one({'_id': related_id})
            related_date = anime.get('aired_from') if anime else 0

            type_diff = get_diff(related_type, anime_date, related_date)

            before_sequence = animes_sequence.get(related_id)
            if before_sequence:
                related_sequence = min(before_sequence, current_sequence + type_diff)
            else:
                related_sequence = current_sequence + type_diff

            animes_sequence[related_id] = related_sequence
            anime_related = anime.get('related') if anime else {}
            if related_id not in open_set:
                open_set.add(related_id)
                if anime_related:
                    set_related(anime_related, related_sequence, related_date, open_set)



for group in group_col.find({'_id': 1441}):
    group_set = group.get('anime')
    group_index = group.get('_id')
    for anime_id in group_set:
        if anime_id not in animes_sequence:
            current_sequence = animes_sequence.get(anime_id, 100)
            anime = col.find_one({'_id': anime_id})
            if anime:
                anime_date = anime.get('aired_from')
                related = anime.get('related', [])
                set_related(related, current_sequence, anime_date, set())


for anime_id, sequence in animes_sequence.items():
    col.update({'_id': anime_id}, {'$set': {'sequence': sequence}})