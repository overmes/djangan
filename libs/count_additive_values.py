import argparse
import pymongo

arguments = argparse.ArgumentParser()
arguments.add_argument('-d', '--db', dest='db', type=str, default='test', help='mongodb for saving results')
arguments.add_argument('--users', dest='users_db', type=str, default='test', help='mongodb with user scores')
args = arguments.parse_args()


con = pymongo.MongoClient()

anime_db = con[args.db]
anime_col = anime_db.anime

users_db = con[args.users_db]
users_col = users_db.users

types = {
    0: "notadded",
    1: "watching",
    2: "completed",
    3: "hold",
    4: "drop",
    6: "plan"}

result_dict = {}
scores_sum = 0.0
scores_count = 0.0
for user_doc in users_col.find():
    scores = user_doc['scores']
    if scores:
        for id, (score, type, date) in scores.items():

            id_obj = result_dict.setdefault(id, {'types': {}})
            obj_types = id_obj['types']

            if score > 0:
                id_obj['scores_count'] = id_obj.get('scores_count', 0.0) + 1
                id_obj['scores_sum'] = id_obj.get('scores_sum', 0.0) + score

                scores_sum += score
                scores_count += 1

            if type in types:
                obj_types[type] = obj_types.get(type, 0) + 1


scores_mean = scores_sum/scores_count
C = 300

counted_result = {}
for id, id_obj in result_dict.items():
    update_dict = counted_result.setdefault(id, {})
    if 'scores_sum' in id_obj:
        update_dict['avg'] = id_obj['scores_sum']/id_obj['scores_count']
        update_dict['weight'] = (C*scores_mean + id_obj['scores_sum'])/(C + id_obj['scores_count'])

    current_types = id_obj['types']
    for type, value in id_obj['types'].items():

        main_sum = current_types.get(1, 0) + current_types.get(2, 0) + current_types.get(3, 0) + current_types.get(4, 0)

        update_dict[types[type]+'_sum'] = value
        if type in [1, 2, 3, 4]:
            update_dict[types[type]+'_percent'] = float(value)/main_sum
        elif type == 6:
            update_dict[types[type]+'_percent'] = float(value)/(main_sum + current_types.get(6, 0))


for id, new_values in counted_result.items():
    int_id = int(id)
    if int_id > 0:
        anime_col.update({'_id.i': int_id, '_id.t': {'$in': ["TV", "Movie", "OVA", "Special", "ONA", "Music"]}},
            {'$set': new_values})
    else:
        anime_col.update({'_id.i': abs(int_id), '_id.t': {'$in': ["Doujin", "Manhwa", "Manhua", "Novel", "One Shot", "Manga"]}},
            {'$set': new_values})

for anime_doc in anime_col.find():
    if 'duration' in anime_doc and anime_doc['duration'] and 'episodes' in anime_doc and anime_doc['episodes']:
        anime_col.update({'_id': anime_doc['_id']}, {'$set': {'length': anime_doc['duration']*anime_doc['episodes']}})