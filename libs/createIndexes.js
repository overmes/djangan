var fields = ["aired_from", "aired_to", "avg", "completed_percent", "completed_sum", "drop_percent", "drop_sum",
    "duration", "episodes", "favorites", "genres", "group", "hold_percent", "hold_sum", "length", "members",
    "members_score", "plan_percent", "plan_sum", "producers", "rating", "scores", "status", "watching_percent",
    "watching_sum", "weight", "authors", "chapters", "serialization", "volumes"];

for( var field in fields){
    var field_name = fields[field];
    print(field_name);
    var index = {};
    index[field_name] = -1;
    db.anime.ensureIndex( index );
}
