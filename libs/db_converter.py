import argparse
import datetime
import pymongo

arguments = argparse.ArgumentParser()
arguments.add_argument('-f', '--from', dest='db_from', type=str, help='source')
arguments.add_argument('-t', '--to', dest='db_to', type=str, help='destination')
args = arguments.parse_args()


con = pymongo.MongoClient()
db_from = con[args.db_from]
db_to = con[args.db_to]

col_from = db_from.anime
col_to = db_to.anime


def format_parent_id(id_list):
    if id_list:
        return format_id(id_list)[0]
    else:
        return None

def format_id(id_list):
    res = []
    for id in id_list:
        if id['t'] == 'anime':
            str(res.append({'anime_id': id['i']}))
        else:
            str(res.append({'anime_id': -id['i']}))
    return res


copy_columns = ['_id', 'type', 'aired_from', 'aired_to', 'status', 'genres', 'related', 'rating', 'title',
                'members_score', 'episodes']
for anime_doc in col_from.find():

    res = {}
    for name, value in anime_doc.items():
        if name == '_id':
            res['type'] = value['t']
            if value['t'] in ["TV", "Movie", "OVA", "Special", "ONA", "Music"]:
                res['_id'] = value['i']
            else:
                res['_id'] = -value['i']
        elif name == 'status':
            res['status'] = value.lower()
        elif name == 'rating':
            res['classification'] = value
        elif name == 'aired_to' and value:
            res['end_date'] = (datetime.datetime.fromtimestamp(value).strftime('%Y-%m-%d %H:%M:%S'))
        elif name == 'aired_from' and value:
            res['start_date'] = (datetime.datetime.fromtimestamp(value).strftime('%Y-%m-%d %H:%M:%S'))
        elif name == 'related':
            res['side_stories'] = format_id(value.get('side story', []))
            res['summaries'] = format_id(value.get('summary', []))
            res['parent_story'] = format_parent_id(value.get('parent story', None))
            res['character_anime'] = format_id(value.get('character', []))
            res['spin_offs'] = format_id(value.get('spin-off', []))
            res['sequels'] = format_id(value.get('sequel', []))
            res['prequels'] = format_id(value.get('prequel', []))
            res['alternative_versions'] = format_id(value.get('alternative version', []))
        elif name in copy_columns:
            res[name] = value

    col_to.save(res)