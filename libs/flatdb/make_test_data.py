import pymongo

con = pymongo.MongoClient()
db_test = con.test
test_col = db_test.fields10

db_animedb = con.anidb0503
anime_col = db_animedb.anime


for i, anime_doc in enumerate(anime_col.find()):
    anime_id = anime_doc['_id']
    test_col.insert({'item': i, 'type': 'malid', 'value': anime_id['i']})
    if anime_id['t']:
        test_col.insert({'item': i,'type': 'type', 'value': anime_id['t']})
    print i
    for key, anime_data in anime_doc.items():
        if anime_data:
            if key != 'user_scores' or key != '_id':
                if type(anime_data) == list:
                    for element in anime_data:
                        test_col.insert({'item': i, 'type': key, 'value': element})
                else:
                    test_col.insert({'item': i, 'type': key, 'value': anime_data})
            if key == 'user_scores':
                for user_id, user_data in anime_data.items():
                    for user_type, value in user_data.items():
                        test_col.insert({'item': i, 'type': '{0}/{1}'.format(user_id, user_type), 'value': value})
