import pymongo

con = pymongo.MongoClient()
db_test = con.test
test_col = db_test.fields

db_animedb = con.anidb0503
anime_col = db_animedb.anime


def insert(i, field, values):
    res = {'i': i, 'f': field, 'v': values}
    test_col.insert(res)

for i, anime_doc in enumerate(anime_col.find()):
    anime_id = anime_doc['_id']
    insert(i, 'malid', {'a': anime_id['i']})
    if anime_id['t']:
        insert(i, 'type', {'a': anime_id['t']})
    print i
    for key, anime_data in anime_doc.items():
        if anime_data:
            if key != 'user_scores' and key != '_id' and key != 'related':
                if type(anime_data) == list:
                    for item in anime_data:
                        insert(i, key, {'a': item})
                else:
                    insert(i, key, {'a': anime_data})
            if key == 'user_scores':
                for user_id, user_data in anime_data.items():
                    for data_type, data_value in user_data.items():
                        insert(i, '{0}/{1}'.format(user_id, data_type), {'a': data_value})