import pymongo

# con = pymongo.MongoClient()
# db = con.anime_data
# anime_col = db.anime
# group_col = db.animegroup
# group_res = db.groupres

fields_types = {
    "status": "inarray",
    "rating": "inarray",
    "type": "inarray",
    "genres": "array",
    "english": "concat",
    "title": "concat",
    "synonyms": "concat",
    "synopsis": "concat",
    "japanese": "concat",
    "_id": "normal",
    "group_id": "normal",
    "scores": "normal",
    "episodes": "normal",
    "members": "normal",
    "members_score": "normal",
    "aired_from": "normal",
    "duration": "normal",
    "aired_to": "normal"
}

def update_group_result(group_result, key, value):
    if fields_types[key] == 'array':
        current = group_result.setdefault(key, [])
        group_result[key] = list(set(current + value))

    if fields_types[key] == 'inarray':
        current = group_result.setdefault(key, [])
        current.append(value)
        group_result[key] = list(set(current))

    if fields_types[key] == 'concat':
        current = group_result.setdefault(key, value)
        if value and current:
            current_word_array = current.split()
            value_word_array = value.split()
            group_result[key] = ' '.join(list(set(current_word_array + value_word_array)))

    if fields_types[key] == 'normal':
        key = 'group_id' if key == '_id' else key
        if value >= 0:
            current = group_result.setdefault(key, [value, value, (0.0, 0)])
            group_result[key] = [min(current[0], value), max(current[1], value),
                                 (current[2][0] + value, current[2][1] + 1)]

for group in group_col.find():
    group_index = group.get('_id')
    anime_group = anime_col.find({'group':group_index})


    group_result = {'_id': group_index}
    for anime in anime_group:
        for key, value in anime.items():
            update_group_result(group_result, key, value) if key in fields_types else None

    for key, value in group_result.items():
        if key != '_id' and fields_types[key] == 'normal':
            value[2] = value[2][0]/value[2][1]

    group_res.save(group_result)