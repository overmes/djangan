import argparse
import pymongo
from quick_union import *

arguments = argparse.ArgumentParser()
arguments.add_argument('-d', '--db', dest='db', type=str, default='test', help='mongodb for saving results')
args = arguments.parse_args()

con = pymongo.MongoClient()
db = con[args.db]
col = db.anime
group_col = db.animegroup

related_exclude = ['character', 'adaptation', 'other']

union_dict = {}
def get_type(anime_type):
    return 'anime' if anime_type in ["TV", "Movie", "OVA", "Special", "ONA", "Music"] else 'manga'


anime_data = list(col.find())
for anime_doc in anime_data:
    current_id = (anime_doc['_id']['i'], get_type(anime_doc['_id']['t']))
    union_dict.setdefault(current_id, current_id)
    if anime_doc['related']:
        for related_type, related_list in anime_doc['related'].items():
            if related_type not in related_exclude:
                for related_id_dict in related_list:
                    related_id = (related_id_dict['i'], related_id_dict['t'])
                    union_dict.setdefault(related_id, related_id)
                    quick_union(union_dict, current_id, related_id)


replace = {}
group_count = 0
for anime_doc in anime_data:
    current_id = (anime_doc['_id']['i'], get_type(anime_doc['_id']['t']))
    root_id = get_root(union_dict, current_id)

    replace_id = replace.get(root_id, 0)
    if not replace_id:
        group_count += 1
        replace_id = group_count
        replace[root_id] = replace_id

    col.update({'_id': anime_doc['_id']}, {'$set': {'group': replace_id}})
