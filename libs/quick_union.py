def get_root(lis, item):
    """
    return the root of an item
    """
    root = item
    while lis[root] != root:
        root = lis[root]
    return root


def connected(lis, item1, item2):
    """
    Checks if two items are connected
    or not.
    Connected means both items have the
    same root
    """
    root1 = get_root(lis, item1)
    root2 = get_root(lis, item2)
    return True if root1 == root2 else False


def quick_union(lis, item1, item2):
    """
    Join two nodes(join their roots)
    """
    root1 = get_root(lis, item1)
    root2 = get_root(lis, item2)
    if root1 != root2:
        lis[root1] = root2
    else:
        print "Already connected"
