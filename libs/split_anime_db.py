import argparse
import pymongo


arguments = argparse.ArgumentParser()
arguments.add_argument('-s', '--source', dest='source', type=str, help='mongodb source')
arguments.add_argument('-d', '--destination', dest='destination', type=str, help='mongodb destination')
args = arguments.parse_args()

con = pymongo.MongoClient()
source_db = con[args.source]
source_col = source_db.anime

dest_db = con[args.destination]
dest_col = dest_db.anime
