from .base import *

DEBUG = True
TEMPLATE_DEBUG = True

MONGODB_HOST = '127.0.0.1'
MONGODB_PORT = 27017
MONGODB_DATABASE = 'mal'

ALLOWED_HOSTS = ['0.0.0.0', '127.0.0.1']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'djangan',
        'USER': 'djangan',
        'PASSWORD': 'jfadkslfjajsfkjl',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}