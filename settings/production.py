from .base import *

DEBUG = False
TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['.animeadvice.me']

MONGODB_HOST = '127.0.0.1'
MONGODB_PORT = 27017
MONGODB_DATABASE = 'anidb0503'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'djangan',
        'USER': 'djangan',
        'PASSWORD': 'klcjbvioduaiotjkr',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

MIDDLEWARE_CLASSES += ('raven.contrib.django.raven_compat.middleware.Sentry404CatchMiddleware',)

INSTALLED_APPS += ('raven.contrib.django.raven_compat',)
