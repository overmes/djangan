define([
    'jquery',
    'underscore',
    'backbone',
    'animelist/App/AnimeModel',
    'animelist/App/QueryModel',
    'backbone_tastypie'
], function($, _, Backbone, AnimeModel, QueryModel){
    return Backbone.Collection.extend({
        model: AnimeModel,
        initialize: function(models, options){
            this.fieldsCollection = options.fieldsCollection;
            this.settingsModel = options.settingsModel;

            this.queryModel = new QueryModel({fieldsCollection: this.fieldsCollection, settingsModel: this.settingsModel});

            this.pageCount = 1;
            this.currentPage = 1;

            this.changeDataSource(this.settingsModel);

            this.queryModel.on('change', this.fetchQuery, this);
            this.settingsModel.on('change:groupView', this.changeDataSource, this);
        },

        changeDataSource: function(model){
            if (model.get('groupView')){
                this.url = '/api/v1/animegrouplist/'
            } else {
                this.url = '/api/v1/animelist/'
            }

        },


        fetchQuery: function(){
            var self = this;
            this.fetch({
                data: this.queryModel.getQuery(),
                reset: true,
                success: function(collection, response, options){
                    self.totalCount = response.meta.total_count;
                    var pageCount = Math.ceil(self.totalCount / self.queryModel.get('query_limit'));
                    self.setPageCount(pageCount);
                }
            });
        },

        setPage: function(page, options){
            this.currentPage = page;
            this.queryModel.set("offset", (this.currentPage - 1) * this.queryModel.get("query_limit"), options);
        },

        setPageCount: function(pageCount){
            var previousPageCount = this.pageCount;

            if(pageCount == 0){
                this.setPage(1);
                this.pageCount = 1;
            }else if( this.currentPage > pageCount){
                this.pageCount = pageCount;
                this.setPage(pageCount);
            }else{
                this.pageCount = pageCount;
            }

            if(previousPageCount != this.pageCount){
                this.trigger('pagecountchange');
            }
        }

    })
});