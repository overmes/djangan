require([
    'animelist/App/Router',
    'ravenjs',
    'jquery_cookie',
    'helpers'
], function (Router) {
    var CSRF_TOKEN = $.cookie('csrftoken');
    $.ajaxSetup({
        headers: {'X-CSRFToken': CSRF_TOKEN}
    });

    //if (DEBUG){
    //     var router = new Router();
    //} else {
    //    Raven.config('http://6d5c51722c0d401498974c0bd61f34fe@sentry.animeadvice.me/2', {
    //        whitelistUrls: [/.*animeadvice\\.me/],
    //        ignoreErrors: []
    //    }).install();
    //    try {
    //        var router = new Router();
    //    } catch(err) {
    //        Raven.captureException(err);
    //    }
    //
    //    $(document).ajaxError( function(e, jqXHR, options, exception){
    //        Raven.captureMessage(jqXHR.statusCode, {tags: { url: options.url, text: jqXHR.statusText}});
    //        return false;
    //    });
    //
    //    var previous = window.onerror;
    //    window.onerror = function (message, file, line, column, errorObj) {
    //        previous(message, file, line, column, errorObj);
    //        Raven.captureMessage(message, {tags: { file: file, line: line}});
    //    }
    //}

    var router = new Router();
    Backbone.history.start();
});