define([
    'jquery',
    'underscore',
    'backbone',
    'moment'
], function($, _, Backbone, moment){
    return Backbone.Model.extend({
        idAttribute: "id",

        initialize: function(){
            this.formatters = this.getFormatters();
        },

        format: function (fieldModel) {
            var type = fieldModel.get("type");
            var field_name = fieldModel.get("name");
            var rawData = undefined;

            if (field_name.indexOf('.') >= 0) {
                var path = field_name.split(".");
                rawData = this.get(path[0]);
                if (rawData) {
                    for (var i = 1; i < path.length; i++){
                        if (path[i] in rawData) rawData = rawData[path[i]];
                    }
                }
            } else {
                rawData = this.get(field_name);
            }
            var format = fieldModel.get("format");
            return this.formatters[format](rawData, this);
        },

        getJSON: function(fields){
            var result = {};
            for (var index in fields) {
                var currentField = fields[index];
                result[currentField.get('name')] = this.format(currentField);
            }
            return result;
        },

        getIdURL: function(id){
            var template = 'http://myanimelist.net/{0}/{1}/';
            if (id['t'] == 'anime' || ["TV", "Movie", "OVA", "Special", "ONA", "Music"].indexOf(id['t']) >= 0){
                return template.format('anime', id['i']);
            } else {
                return template.format('manga', id['i']);
            }
        },

        getURL: function(){
            return this.getIdURL(this.get('_id'));
        },

        getMediaFormat: function(field){
            var labelInfo = '<span class="label label-info">{0}</span>';
            var simpleDiv = '<div>{0}</div>';
            var labelDefault = '<span class="label label-default">{0}</span>';
            var labelDanger = '<span class="label label-danger">{0}</span>';
            var fieldName = field.get('name');

            var formattedField = this.format(field);

            if (['duration', 'volumes', 'chapters', 'episodes', 'length'].indexOf(fieldName) >= 0){
                if (formattedField) {
                    return labelInfo.format(field.get('short') + ': ' + formattedField);
                }
            }

            if (['_id.i'].indexOf(fieldName) >= 0){
                if (formattedField) {
                    return labelDefault.format(field.get('short') + ': ' + formattedField);
                }
            }

            if (['count'].indexOf(fieldName) >= 0){
                if (formattedField) {
                    return labelDefault.format(field.get('short') + ' ' + formattedField);
                }
            }

            if (['_id.t'].indexOf(fieldName) >= 0){
                if (formattedField) {
                    return labelDanger.format(formattedField);
                }
            }

            if (['synopsis'].indexOf(field.get('name')) >= 0){
                return simpleDiv.format(formattedField);
            }

            return ''
        },

        getFormatters: function () {
            return {
                count: function (rawData, animeModel) {
                    var collection = animeModel.collection;
                    var limit = collection.queryModel.get('query_limit');
                    var page = collection.currentPage - 1;
                    var index = collection.models.indexOf(animeModel) + 1;
                    return index + page*limit;
                },
                date: function (rawData, animeModel) {
                    if (rawData != undefined) {
                        var day = moment.unix(rawData);
                        return day.isValid() ? day.format('MMM YYYY'):'';
                    } else {
                        return '';
                    }
                },

                list: function (rawData, animeModel) {
                    if (rawData != undefined) {
                        return rawData.join(' ');
                    } else {
                        return '';
                    }
                },

                image: function (rawData, animeModel) {
                    if (rawData != undefined && rawData.indexOf('na_series.gif') < 0) {
                        return '<img src="' + rawData + '">';
                    } else {
                        return '';
                    }
                },

                thumb: function (rawData, animeModel) {
                    if (rawData != undefined && rawData.indexOf('na_series.gif') < 0) {
                        return '<img src="' + rawData.replace('.jpg', 't.jpg') + '">';
                    } else {
                        return '';
                    }
                },

                string: function (rawData, animeModel) {
                    if (rawData != undefined) {
                        return rawData;
                    } else {
                        return '';
                    }
                },

                related: function (rawData, animeModel) {
                    if (rawData != undefined) {
                        if (typeof rawData == 'string') {
                            return rawData;
                        } else {
                            var url_template = '<a target="blank" href="{0}">{1}</a><br>';
                            var result = '';
                            for (var rel_type in rawData) {
                                result += rel_type + ':<br> ';
                                for (var index in rawData[rel_type]) {
                                    var current_id = rawData[rel_type][index];
                                    result += url_template.format(animeModel.getIdURL(current_id), current_id['title']) + ' ';
                                }
                            }
                            return result;
                        }
                    } else {
                        return '';
                    }
                },

                float: function (rawData, animeModel) {
                    if (rawData != undefined) {
                        return rawData % 1 > 0 ? rawData.toFixed(2) : rawData;
                    } else {
                        return '';
                    }
                },

                anime_url: function (rawData, animeModel) {
                    var template = '<a target="blank" href="{0}">{1}</a>';
                    if (rawData){
                        return template.format(animeModel.getURL(), rawData);
                    } else {
                        return '';
                    }
                },

                user_score: function (rawData, animeModel) {
                    if (rawData != undefined && rawData > 0) {
                        return rawData || '';
                    } else {
                        return '';
                    }
                },

                user_status: function (rawData, animeModel) {
                    var type_id = {
                        0: "Not added",
                        1: "Watching",
                        2: "Completed",
                        3: "On-hold",
                        4: "Dropped",
                        6: "Plan"};
                    if (rawData != undefined) {
                        return type_id[rawData];
                    } else {
                        return '';
                    }
                },

                status: function (rawData, animeModel) {
                    var change = {
                        "Not yet aired": "Not aired",
                        "Currently Airing": "Airing",
                        "Finished Airing": "Finished Airing",
                        "Not yet published": "Not published",
                        "Publishing": "Publishing",
                        "Finished": "Finished Publishing"
                    };
                    if (rawData != undefined) {
                        return change[rawData];
                    } else {
                        return '';
                    }
                }
            };
        }
    })
});