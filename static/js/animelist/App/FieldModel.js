define([
    'jquery',
    'underscore',
    'backbone',
    'animelist/App/AnimeModel'
], function ($, _, Backbone, AnimeModel) {
    return Backbone.Model.extend({
        idAttribute: "id",

        initialize: function(){
            this.formatter = AnimeModel.prototype.getFormatters()[this.get('format')];
        },

        defaults: function(){
            return {
                name: "name",
                label: "label",
                type: "type",
                short: "short",
                selected: false
            };
        },

        formatFilter: function(val){
            switch (this.get('filter_type')){
                case 'range':
                    return "{0} - {1}".format(val.from, val.to);
                default :
                    return val;
            }

        },

        getQueryObject: function(){
            var field_filter = this.get('filter');
            switch (this.get('filter_type')) {
                case 'range':
                    return {$gte: field_filter.from, $lte: field_filter.to};

                case 'select':
                    var select_list = field_filter.slice(0);
                    if (this.get('name') == 'related') {
                        var related_list = this.get('filter_opt')['values'];
                        var and = [];
                        for (var i in related_list) {
                            var current_related = related_list[i];
                            if (select_list.indexOf(current_related) < 0) {
                                var field_name = 'related.' + current_related;
                                var current_and = {}
                                current_and[field_name] = null;
                                and.push(current_and);
                            }
                        }

                        if (and.length > 0) {
                            return {$and: and};
                        } else {
                            return null;
                        }
                    } else {
                        if( this.get('type') == 'user') select_list[select_list.indexOf(0)] = null;
                        if(this.get('and')){
                            return {$all: select_list};
                        } else {
                            return {$in: select_list};
                        }
                    }

                case 'ahead':
                    if(this.get('and')){
                        return {$all: field_filter};
                    } else {
                        return {$in: field_filter};
                    }

                case 'genres':
                    var include = field_filter.include;
                    var exclude = field_filter.exclude;
                    var result_filter = {};

                    if (include.length > 0) {
                        if(this.get('and')){
                            result_filter["$all"] = include;
                        } else {
                            result_filter["$in"] = include;
                        }
                    }

                    if (exclude.length > 0) result_filter["$nin"] = exclude;
                    return result_filter;

                case 'daterange':
                    var from = field_filter.from;
                    var to = field_filter.to;
                    var dateRangeFilter = {};

                    if (from) dateRangeFilter['$gte'] = moment(from).unix();
                    if (to) dateRangeFilter['$lt'] = moment(to).add('days', 1).unix();

                    return dateRangeFilter;

                case 'text':
                    if(field_filter == "") return null;

                    var regexQuery = "";
                    var splitedString = field_filter.split(' ');
                    _.each(splitedString, function(text){
                        regexQuery += "(?=.*{0})".format(text);
                    });

                    return { $regex: regexQuery, $options: 'i' } ;

                default :
                    return null
            }
        }



    });
});
