define([
    'jquery',
    'underscore',
    'backbone',
    'animelist/FieldsSelect/FieldsSelectView',
    'animelist/Table/TableView',
    'animelist/Pagination/PaginationView',
    'animelist/UserSelect/UserSelectView',
    'animelist/SaveState/SaveStateView',
    'animelist/Stats/StatsView',
    'animelist/Settings/SettingsView',
    'animelist/DBUpdate/DBUpdateView',
    'animelist/App/AnimeCollection',
    'animelist/FieldsSelect/FieldsCollection',
    'text!animelist/App/NormalTemplate.html',
    'text!animelist/App/FluidTemplate.html'
], function($, _, Backbone, FieldsSelectView, TableView, PaginationView, UserSelectView, SaveStateView, StatsView,
            SettingsView, DBUpdateView, AnimeCollection, FieldsCollection, NormalTemplate, FluidTemplate){
    return Backbone.View.extend({

        initialize: function(){
            this.settingsView = new SettingsView({name: 'Settings'});

            this.fieldsCollection = new FieldsCollection();
            this.primaryFieldsSelectView = new FieldsSelectView({name: 'Main Fields', 'class': 1, fieldsCollection: this.fieldsCollection});
            this.secondaryFieldsSelectView = new FieldsSelectView({name: 'Support Fields', 'class': 2, fieldsCollection: this.fieldsCollection});

            this.animeCollection = new AnimeCollection([], {fieldsCollection: this.fieldsCollection,
                                                        settingsModel: this.settingsView.settingsModel});

            this.tableView = new TableView({fieldsCollection: this.fieldsCollection,
                                            settingsModel: this.settingsView.settingsModel,
                                            animeCollection: this.animeCollection});

            this.statsView = new StatsView({name: 'Statistics', queryModel: this.animeCollection.queryModel});

            this.userSelectView = new UserSelectView({name: 'User scores', fieldsCollection: this.fieldsCollection});

            this.saveStateView = new SaveStateView({name: 'Save state', fieldsCollection: this.fieldsCollection,
                animeCollection: this.animeCollection, userRequestModel: this.userSelectView.userRequestModel,
                settingsView: this.settingsView});

            this.dbupdateView = new DBUpdateView({name: 'Updates'});

            this.paginationView = new PaginationView({animeCollection: this.animeCollection});

            $('#main').html(this.$el);
            this.render();
            this.renderSubElements();

            this.settingsView.settingsModel.on('change:fluidView', this.render, this);

        },

        addDefaultFields: function(){
            this.fieldsCollection.addDefaultFields();
        },

        setState: function(state){
            this.saveStateView.setState(state);
        },

        render: function(){
            var template;
            this.$el.children().detach();
            if (this.settingsView.settingsModel.get('fluidView')) {
                template = _.template(FluidTemplate);
                this.$el.html(template());
            } else {
                template = _.template(NormalTemplate);
                this.$el.html(template());
            }
            this.addSubElements();
            return this;
        },

        renderSubElements: function(){
            this.primaryFieldsSelectView.render();
            this.secondaryFieldsSelectView.render();
            this.secondaryFieldsSelectView.hidePanel();
            this.userSelectView.render();
            this.tableView.render();
            this.paginationView.render();
            this.saveStateView.render();
            this.statsView.render();
            this.settingsView.render();
            this.dbupdateView.render();
        },

        addSubElements: function(){
            this.$('#primary-fields-select').html(this.primaryFieldsSelectView.$el);
            this.$('#secondary-fields-select').html(this.secondaryFieldsSelectView.$el);
            this.$('#user-select').html(this.userSelectView.$el);
            this.$('#grid').html(this.tableView.$el);
            this.$('#paginator').html(this.paginationView.$el);
            this.$('#save-state').html(this.saveStateView.$el);
            this.$('#stats').html(this.statsView.$el);
            this.$('#settings').html(this.settingsView.$el);
            this.$('#dbupdate').html(this.dbupdateView.$el);
        }

    })
});