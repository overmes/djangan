define([
    'jquery',
    'underscore',
    'backbone',
    'moment'
], function($, _, Backbone){
    return Backbone.Model.extend({
        initialize: function(options) {
            this.fieldsCollection = options.fieldsCollection;
            this.settingsModel = options.settingsModel;

            this.setSettings(this.settingsModel);

            this.settingsModel.on('change', this.changeSettings, this);

            this.fieldsCollection.on("add", this.onAddField, this);
            this.fieldsCollection.on("change:selected", this.onChangeSelected, this);
            this.fieldsCollection.on("change:filter change:ornull change:and", this.onChangeFilter, this);
            this.fieldsCollection.on("change:sort change:group_sort change:date", this.fireChange, this);
            this.fieldsCollection.on("change:name", this.onChangeName, this);

            var self = this;
            this.waitChangeFunction = _.debounce(function(){
                self.trigger('change', self);
            }, 300);

        },

        defaults: {
            filters: {},
            direction: 1,
            limit: 0,
            query_limit: 100,
            group_limit: 100,
            sort_type: 'avg',
            groupView: false,
            offset: 0
        },

        setSettings: function(model){
            this.set(model.toJSON(), {silent: true});
        },

        changeSettings: function(model, value, options){
            this.setSettings(model);

            var group_settings = ['group_limit', 'sort_type'];
            var changed_field = Object.keys(model.changed)[0];

            if (group_settings.indexOf(changed_field) >= 0){
                if (model.get('groupView')) this.waitChangeFunction();
            } else {
                this.waitChangeFunction();
            }
        },

        fireChange: function() {
            this.trigger('change', this);
        },

        getQuery: function() {
            var res = {
                filters: JSON.stringify(this.get('filters')),
                fields: JSON.stringify(this.fieldsCollection.getSelectedNamesList()),
                limit: this.get('limit'),
                query_limit: this.get('query_limit'),
                group_limit: this.get('group_limit'),
                sort_type: this.get('sort_type'),
                skip: this.get('offset')
            };

            var sortField = this.fieldsCollection.getSortField();
            if (sortField) res['sort'] = JSON.stringify([[sortField.get('name'), sortField.get('sort')]]);

            var sortGroupField = this.fieldsCollection.getGroupSortField();
            if (sortGroupField) res['group_sort'] = JSON.stringify([[sortGroupField.get('name'), sortGroupField.get('group_sort')]]);
            return res;
        },

        onChangeSelected: function(model, value, options) {
            this.waitChangeFunction();
        },

        onAddField: function(model, value, options){
            if (model.get('filter')) {
                this.setFilter(model);
            }
            this.waitChangeFunction();
        },

        onChangeName: function(model, value, options) {
            if (model.get('selected') && model.changed.date) {
                this.waitChangeFunction();
            }
        },

        setFieldFilter: function(field){
            var field_name = field.get('name');
            var queryObject = field.getQueryObject();
            var filter = {};
            if (field_name == 'related') {
                filter = queryObject;
            }  else {
                filter[field_name] = queryObject;
            }
            if (field.get('ornull')) {
                var nullFilter = {};
                nullFilter[field_name] = null;
                return {$or: [filter, nullFilter]};
            } else {
                return filter;
            }
        },

        setFilter: function(model){
            var filters = this.fieldsCollection.map(function(field){
                if (field.get('filter')){
                    return this.setFieldFilter(field);
                }
                return null;
            }, this);

            filters = _.filter(filters, function(filter){ return filter});

            if (filters.length > 0) this.set('filters', {$and: filters}, {silent: true});
            else this.set('filters', {}, {silent: true});
        },

        onChangeFilter: function(model, value, options) {
            this.setFilter(model);
            this.waitChangeFunction();
        }
    })
});
