define([
    'jquery',
    'underscore',
    'backbone',
    'animelist/App/PageView'
], function ($, _, Backbone, PageView) {
        return Backbone.Router.extend({

            initialize: function(){
                this.pageView = new PageView();
            },

            routes: {
                "state/:state": "state",
                "main": "main",
                "*default": "index"

            },

            main: function(){
                console.log('main');
                this.pageView.addDefaultFields();
            },

            index: function (url) {
                console.log(url);
                this.pageView.addDefaultFields();
                if( url){
                    this.navigate('index');
                }
            },

            state: function (state) {
                console.log(state);
                this.pageView.setState(state);
            }

        });
    }
);