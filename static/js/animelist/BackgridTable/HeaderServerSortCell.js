define(['backgrid'], function (Backgrid) {
    return Backgrid.HeaderCell.extend({
        sort: function (columnName, direction, comparator) {
            var order;
            if (direction === "ascending") order = -1;
            else if (direction === "descending") order = 1;
            else order = null;

            var new_order = [];
            if( order != null){
                new_order.push([columnName, order])
            }
            this.collection.query_model.set('sort', new_order);
            this.collection.trigger("backgrid:sort", columnName, direction, comparator, this.collection);
        }

    })
});