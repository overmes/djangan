define(['backgrid'], function (Backgrid) {
    var formatter = {
        fromRaw: function (rawData) {
            if (rawData != undefined) {
                return '<img src="' + rawData + '">';
            } else {
                return '';
            }
        }
    };
    return Backgrid.Cell.extend({
        render: function () {
            this.$el.empty();
            this.$el.html(this.formatter.fromRaw(this.model.get(this.column.get("name"))));
            this.delegateEvents();
            return this;
        },
        className: "img-cell",
        formatter: formatter
    });
});