define(['backgrid'], function (Backgrid) {
    var formatter = {
        fromRaw: function (rawData) {
            if( rawData != undefined){
                return rawData.join(' ');
            }else{
                return '';
            }
        }
    };
    return Backgrid.Cell.extend({
        formatter: formatter
    });
});