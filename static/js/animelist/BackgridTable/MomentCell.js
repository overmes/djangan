define(['backgrid', 'moment'], function (Backgrid, moment) {
    var formatter = {
        fromRaw: function (rawData) {
            if( rawData != undefined){
                var day = moment.unix(rawData);
                return day.format('MMM YYYY');
            }else{
                return '';
            }
        }
    };
    return Backgrid.Cell.extend({
        formatter: formatter
    });
});