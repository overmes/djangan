define([
    'backbone',
    'backgrid',
    'animelist/cells/MomentCell',
    'animelist/cells/ListCell',
    'animelist/cells/ImageCell',
    'animelist/cells/HeaderServerSortCell'
], function(Backbone, Backgrid, MomentCell, ListCell, ImageCell, HeaderServerSortCell){
    return Backbone.View.extend({
        initialize: function(){
            this.filter = this.collection.query_model;

            this.columns = {
                '_id': {
                    name: "_id",
                    label: "ID",
                    editable: false,
                    cell: "integer",
                    headerCell: HeaderServerSortCell
                },
                'title':{
                    name: "title",
                    label: "Name",
                    editable: false,
                    cell: "string",
                    headerCell: HeaderServerSortCell
                },
                aired_from: {
                    name: "aired_from",
                    label: "From",
                    editable: false,
                    cell: MomentCell,
                    headerCell: HeaderServerSortCell
                },
                aired_to: {
                    name: "aired_to",
                    label: "To",
                    editable: false,
                    cell: MomentCell,
                    headerCell: HeaderServerSortCell
                },
                'duration': {
                    name: "duration",
                    label: "Duration",
                    editable: false,
                    cell: "integer",
                    headerCell: HeaderServerSortCell
                },
                'episodes': {
                    name: "episodes",
                    label: "Ep",
                    editable: false,
                    cell: "integer",
                    headerCell: HeaderServerSortCell
                },
                'genres': {
                    name: "genres",
                    label: "Genres",
                    editable: false,
                    cell: ListCell,
                    headerCell: HeaderServerSortCell
                },
                'image': {
                    name: "image",
                    label: "Img",
                    editable: false,
                    cell: ImageCell,
                    headerCell: HeaderServerSortCell
                },
                'members_score': {
                    name: "members_score",
                    label: "Score",
                    cell: "number",
                    headerCell: HeaderServerSortCell
                },
                'rating':{
                    name: "rating",
                    label: "Rating",
                    editable: false,
                    cell: "string",
                    headerCell: HeaderServerSortCell
                },
                'status':{
                    name: "status",
                    label: "Status",
                    editable: false,
                    cell: "string",
                    headerCell: HeaderServerSortCell
                },
                'synopsis':{
                    name: "synopsis",
                    label: "Synopsis",
                    editable: false,
                    cell: "string",
                    headerCell: HeaderServerSortCell
                },
                'type':{
                    name: "type",
                    label: "Type",
                    editable: false,
                    cell: "string",
                    headerCell: HeaderServerSortCell
                }
            };
            this.filter.on('change', this.render, this);
        },

        render: function(){
            var grid = this.getGrid();
            this.$el.html(grid.render().$el);
            return this;
        },

        getGrid: function(){
            var columns = [];
            _.each( this.filter.fields.available_fields, function(field){
                if( this.collection.query_model.fields.get(field) == 1){
                    columns.push(this.columns[field]);
                }
            }, this);

            var grid = new Backgrid.Grid({
              columns: columns,
              collection: this.collection
            });
            return grid;
        }
    })
});