define([
    'backbone',
    'animelist/DBUpdate/DBUpdateModel'
], function(Backbone, DBUpdateModel){
    return Backbone.Collection.extend({
        model: DBUpdateModel,
        url: '/api/v1/dbupdate/',
    });
});