define([
    'backbone',
    'moment'
], function(Backbone, moment){
    return Backbone.Model.extend({
        url: '/api/v1/dbupdate/',
        parse: function(response, options) {
            response['date'] = moment(response['date']).format('DD MMMM YYYY');
            return response;
        }
    });
});