define([
    'backbone',
    'animelist/Panel/AbstractPanelView',
    'animelist/DBUpdate/DBUpdateCollection',
    'text!animelist/DBUpdate/DBUpdateTemplate.html'
], function(Backbone, AbstractPanelView, DBUpdateCollection, DBUpdateTemplate){
    return AbstractPanelView.extend({
        initialize: function(options){
            AbstractPanelView.prototype.initialize.call(this, options);
            this.hidePanel();
            this.collection = new DBUpdateCollection();
            this.collection.on('reset', this.render, this);
            this.collection.fetch({reset: true});
        },

        events: function(){
            return _.extend({},AbstractPanelView.prototype.events,{
            })
        },

        render: function(){
            console.log(this.collection);
            var template = _.template(DBUpdateTemplate);
            this.$el.find('.content').html(template({collection: this.collection}));
            return this;
        }
    })
});