define([
    'jquery',
    'underscore',
    'backbone',
    'text!animelist/FieldsSelect/Fields/AbstractFieldTemplate.html',
    'modelbinder'
], function($, _, Backbone, AbstractFieldTemplate){
    return Backbone.View.extend({
        initialize: function(){
            this._modelBinder = new Backbone.ModelBinder();
            this.model.on('remove', this.onRemove, this);

            var template = _.template(AbstractFieldTemplate);
            this.setElement( template({field: this.model}));

            if( this.model.get('filter')){
                this.filter_hidden = false;
            }else{
                this.filter_hidden = true;
            }
        },
        events: {
            "click .field-filter-icon" : 'onFilterIconClick',
            "click .field-filter-reset" : 'resetFilters'
        },

        render: function(){
            this._modelBinder.bind(this.model, this.$el);
            return this;
        },

        showResetIcon: function(){
            this.$(".field-filter-reset").show();
        },

        hideResetIcon: function(){
            this.$(".field-filter-reset").hide();
        },

        resetFilters: function(event){
            event.preventDefault();
            this.hideResetIcon();
            this.hideFilter();
            this.model.unset('filter');
        },

        showFilter: function(){
            this.$el.find('.field-filter').show();
            this.$el.find('.field-filter-icon').removeClass('glyphicon-chevron-down');
            this.$el.find('.field-filter-icon').addClass('glyphicon-chevron-up');
            this.filter_hidden = false;
        },

        hideFilter: function(){
            this.$el.find('.field-filter').hide();
            this.$el.find('.field-filter-icon').removeClass('glyphicon-chevron-up');
            this.$el.find('.field-filter-icon').addClass('glyphicon-chevron-down');
            this.filter_hidden = true;
        },

        onFilterIconClick: function(event){
            event.preventDefault();

            if( this.filter_hidden){
                this.showFilter();
            }else{
                this.hideFilter();
            }

        },

        onRemove: function(){
            this.$el.remove();
        }
    })
});