define([
    'jquery',
    'underscore',
    'backbone',
    'animelist/FieldsSelect/Fields/AbstractFieldView',
    'text!animelist/FieldsSelect/Fields/DateRangeSelectTemplate.html',
    'bootstrap',
    'datepicker',
    'moment'
], function ($, _, Backbone, AbstractFieldView, DateRangeSelectTemplate) {
    'use strict';
    return AbstractFieldView.extend({
        initialize: function () {
            AbstractFieldView.prototype.initialize.apply(this);
        },

        events: function () {
            return _.extend({}, AbstractFieldView.prototype.events,{
            });
        },

        onChangeSelect: function (self) {
            this.showResetIcon();
            var fromValue = this.$('[name=start]').val(),
                toValue = this.$('[name=end]').val();

            if (fromValue === "" && toValue === "") {
                self.model.unset('filter');
            } else {
                self.model.set('filter', {
                    from: fromValue,
                    to: toValue
                });
            }

        },

        render: function () {
            AbstractFieldView.prototype.render.apply(this);
            var template = _.template(DateRangeSelectTemplate);
            this.$('.field-filter').prepend(template({field: this.model}));
            this.initSelect();
            if (!this.model.get('filter')) {
                this.hideFilter();
                this.hideResetIcon();
            }
            return this;
        },

        initSelect: function () {
            var self = this;
            this.$('.input-daterange').datepicker({
                autoclose: true,
                todayHighlight: true,
                startView: 1,
                minViewMode: 0
            }).on('changeDate', function () {self.onChangeSelect(self); });
        },

        resetFilters: function (event) {
            AbstractFieldView.prototype.resetFilters.apply(this, [event]);
            this.$('.input-daterange input').val('');
            this.$('.input-daterange input').datepicker('update');
        }
    });
});