define([
    'jquery',
    'underscore',
    'backbone',
    'animelist/FieldsSelect/Fields/AbstractFieldView',
    'text!animelist/FieldsSelect/Fields/GenresSelectTemplate.html',
    'bootstrap',
    'bootstarpselect'
], function($, _, Backbone, AbstractFieldView, GenresSelectTemplate){
    return AbstractFieldView.extend({
        initialize: function(){
            AbstractFieldView.prototype.initialize.apply(this);
        },

        events: function(){
            return _.extend({},AbstractFieldView.prototype.events,{
                'change select': 'onChangeSelect'
            })
        },

        onChangeSelect: function(){
            this.showResetIcon();
            var genres_include = [];
            this.$( ".genres-include option:selected").each(function(index, option){
                    var val = option.value;
                    genres_include.push(val);
                }

            );

            var genres_exclude = [];
            this.$( ".genres-exclude option:selected").each(function(index, option){
                    var val = option.value;
                    genres_exclude.push(val);
                }

            );

            if( genres_include.length == 0 && genres_exclude == 0){
                this.model.unset('filter');
            }else{
                this.model.set('filter', {
                    include: genres_include,
                    exclude: genres_exclude
                });
            }
        },

        render: function(){
            AbstractFieldView.prototype.render.apply(this);
            var template = _.template(GenresSelectTemplate);
            this.$('.field-filter').prepend( template({field: this.model}));
            this.initSelect();
            if( !this.model.get('filter')){
                this.hideFilter();
                this.hideResetIcon();
            }
            return this;
        },

        initSelect: function(){
            this.$('.selectpicker').selectpicker();
        },

        resetFilters: function(event){
            AbstractFieldView.prototype.resetFilters.apply(this, [event]);
            this.$('.selectpicker').selectpicker('deselectAll');
        }
    })
});