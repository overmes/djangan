define([
    'jquery',
    'underscore',
    'backbone',
    'animelist/FieldsSelect/Fields/AbstractFieldView',
    'text!animelist/FieldsSelect/Fields/MultiSelectTemplate.html',
    'bootstrap',
    'bootstarpselect'
], function($, _, Backbone, AbstractFieldView, MultiSelectTemplate){
    return AbstractFieldView.extend({
        initialize: function(){
            AbstractFieldView.prototype.initialize.apply(this);
        },

        events: function(){
            return _.extend({},AbstractFieldView.prototype.events,{
                'change select': 'onChangeSelect',
                'click .fast-select': 'onFastSelect'
            })
        },

        onFastSelect: function(event){
            var fastSelect = event.currentTarget.dataset.key;
            var values = this.model.get('filter_opt')['fast'][fastSelect];
            this.$('.selectpicker').selectpicker('val', values);
            this.$('.selectpicker').selectpicker('render');
        },

        onChangeSelect: function(event){
            this.showResetIcon();
            var selected_options = [];
            var self = this;
            this.$( "select option:selected").each(function(index, option){
                    var val = option.value;
                    if( self.isNumber(val)){
                        val = parseInt(val);
                    }
                    selected_options.push(val);
                }

            );
            this.model.set('filter', selected_options);

        },

        isNumber: function(n) {
          return !isNaN(parseFloat(n)) && isFinite(n);
        },

        render: function(){
            AbstractFieldView.prototype.render.apply(this);
            var template = _.template(MultiSelectTemplate);
            this.$('.field-filter').prepend( template({field: this.model}));
            this.initSelect();
            if( !this.model.get('filter')){
                this.hideFilter();
                this.hideResetIcon();
            }
            return this;
        },

        initSelect: function(){
            this.$('.selectpicker').selectpicker();
        },

        resetFilters: function(event){
            AbstractFieldView.prototype.resetFilters.apply(this, [event]);
            this.$('.selectpicker').selectpicker('selectAll');
        }
    })
});