define([
    'jquery',
    'underscore',
    'backbone',
    'animelist/FieldsSelect/Fields/AbstractFieldView',
    'text!animelist/FieldsSelect/Fields/RangeSelectTemplate.html',
    'ion.rangeSlider'
], function($, _, Backbone, AbstractFieldView, RangeSelectTemplate){
    return AbstractFieldView.extend({
        initialize: function(){
            AbstractFieldView.prototype.initialize.apply(this);
        },

        events: function(){
            return _.extend({},AbstractFieldView.prototype.events,{
                'click .range-edit-icon': 'onIconEditClick',
                'click .range-choose-icon': 'onIconEditClick',
                'keyup .range-edit': 'onRangeEditKeyUp'
            })
        },

        onRangeEditKeyUp: function(event){
            this.showResetIcon();
            var from = +(this.$('[name="From"]').val() || this.model.get('filter_opt')['min']);
            var to = +(this.$('[name="To"]').val() || this.model.get('filter_opt')['max']);

            this.model.set('filter', {from:from, to:to});
        },

        onIconEditClick: function(event) {
            this.$('.range-edit').toggle();
            this.$('.range-choose').toggle();

            if (this.model.get('filter')){
                this.updateRange(this.model.get('filter')['from'], this.model.get('filter')['to']);
            }
        },

        updateRange: function(setFrom, setTo) {
            var min = this.model.get('filter_opt').min;
            var max = this.model.get('filter_opt').max;
            var from = setFrom || min;
            var to = setTo || max;
            var step = this.model.get('filter_opt').step || 1;
            this.$(".range-slider").ionRangeSlider("update", {
                min: min,
                max: max,
                from: from,
                to: to,
                step: step
            });
        },

        showFilter: function(event){
            AbstractFieldView.prototype.showFilter.apply(this);

            if( this.need_update){
                this.updateRange();
                this.need_update = false;
            }

        },

        render: function(){
            AbstractFieldView.prototype.render.apply(this);
            var template = _.template(RangeSelectTemplate);
            this.$('.field-filter').prepend( template({field: this.model}));
            this.initSlider();
            if( !this.model.get('filter')){
                this.hideFilter();
                this.hideResetIcon();
            }
            return this;
        },

        initSlider: function(){
            var self = this;
            this.$(".range-slider").ionRangeSlider({
                onFinish: function(slider){
                    self.showResetIcon();
                    self.$('[name="From"]').val(slider.fromNumber);
                    self.$('[name="To"]').val(slider.toNumber);
                    self.model.set('filter', {from:slider.fromNumber, to:slider.toNumber});
                }
            });
        },

        resetFilters: function(event){
            AbstractFieldView.prototype.resetFilters.apply(this, [event]);
            this.need_update = true;
            this.$('[name="From"]').val('');
            this.$('[name="To"]').val('');
            this.$('.range-edit').hide();
            this.$('.range-choose').show();
        }
    })
});