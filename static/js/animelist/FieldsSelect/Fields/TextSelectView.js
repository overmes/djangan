define([
    'jquery',
    'underscore',
    'backbone',
    'animelist/FieldsSelect/Fields/AbstractFieldView',
    'text!animelist/FieldsSelect/Fields/TextSelectTemplate.html'
], function($, _, Backbone, AbstractFieldView, TextSelectTemplate){
    return AbstractFieldView.extend({
        initialize: function(){
            AbstractFieldView.prototype.initialize.apply(this);
        },

        events: function(){
            return _.extend({},AbstractFieldView.prototype.events,{
                'keyup .text-search': 'onChangeSelect'
            })
        },

        onChangeSelect: function(event){
            this.showResetIcon();
            var text = this.$('.text-search').val();
            if(text == ""){
                 this.model.unset('filter');
            }else{
                this.model.set('filter', text);
            }

        },

        render: function(){
            AbstractFieldView.prototype.render.apply(this);
            var template = _.template(TextSelectTemplate);
            this.$('.field-filter').prepend( template({field: this.model}));
            if( !this.model.get('filter')){
                this.hideFilter();
                this.hideResetIcon();
            }
            return this;
        },

        resetFilters: function(event){
            AbstractFieldView.prototype.resetFilters.apply(this, [event]);
            this.$('.text-search').val('');
        }
    })
});