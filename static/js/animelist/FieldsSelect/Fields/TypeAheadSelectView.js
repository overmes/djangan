define([
    'jquery',
    'underscore',
    'backbone',
    'animelist/FieldsSelect/Fields/AbstractFieldView',
    'text!animelist/FieldsSelect/Fields/TypeAheadSelectTemplate.html',
    'select2'
], function ($, _, Backbone, AbstractFieldView, TypeAheadSelectTemplate) {
    return AbstractFieldView.extend({
        initialize: function () {
            AbstractFieldView.prototype.initialize.apply(this);
        },

        events: function () {
            return _.extend({}, AbstractFieldView.prototype.events, {
            })
        },

        onSelect: function (result) {
            this.showResetIcon();
            var filter = this.$(".text-search").select2("val");
            if (filter.length > 0) {
                this.model.set('filter', filter);
            } else {
                this.model.unset('filter');
            }
        },

        render: function () {
            AbstractFieldView.prototype.render.apply(this);
            var template = _.template(TypeAheadSelectTemplate);
            this.$('.field-filter').prepend(template({field: this.model}));
            if (!this.model.get('filter')) {
                this.hideFilter();
                this.hideResetIcon();
            }
            this.initSelect();
            return this;
        },

        initSelect: function () {
            var field = this.model.get('name');
            var self = this;
            this.$(".text-search").select2({
                multiple: true,
                placeholder: "Add value",
                minimumInputLength: 1,
                separator: ';',
                ajax: {
                    url: "/api/v1/typelist/",
                    dataType: 'json',
                    data: function (term, page) {
                        return {
                            q: term,
                            field: field,
                            limit: 0
                        };
                    },
                    results: function (data, page) {
                        var result = _.map(data['objects'], function (element) {
                            return {id: element['_id'], text: element['_id']};
                        });
                        return {results: result};
                    }
                },
                initSelection: function (element, callback) {
                    var filter = self.model.get('filter');
                    var result = _.map(filter, function (element) {
                        return {id: element, text: element};
                    });
                    callback(result);
                }
            }).on('change', $.proxy(this.onSelect, this));
        },

        resetFilters: function (event) {
            AbstractFieldView.prototype.resetFilters.apply(this, [event]);
            this.$('.text-search').val('');
            this.$(".text-search").select2("val", "");
        }
    })
});