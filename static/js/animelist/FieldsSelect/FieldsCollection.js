define([
    'jquery',
    'underscore',
    'backbone',
    'animelist/App/FieldModel',
    'animelist/FieldsSelect/DefaultFields'
],
    function($, _, Backbone, FieldModel, DefaultFields){
    return Backbone.Collection.extend({
        model: FieldModel,

        initialize: function(){
        },

        comparator: function(field){
            return field.get('class')*1000+field.get('order');
        },

        getSelectedModelsList: function(){
            return this.filter(function(fieldModel){ return fieldModel.get("selected")});
        },

        getSelectedNamesList: function(){
            return _.map(this.getSelectedModelsList(), function(field){return field.get('name')});
        },

        getSortField: function(){
            return this.filter(function(model){
                return model.get('sort');
            })[0];
        },

        getGroupSortField: function(){
            return this.filter(function(model){
                return model.get('group_sort');
            })[0];
        },

        addDefaultFields: function(){
            this.set(DefaultFields);
        },

        setSortField: function(newSortField){
            var currentSort = this.getSortField();
            if( newSortField == currentSort){
                newSortField.set('sort', newSortField.get('sort') > 0 ? -1 : 1);
            }else{
                if( currentSort) currentSort.unset('sort', {silent: true});
                newSortField.set('sort', -1);
            }
        },

        setGroupSortField: function(newSortField){
            var currentSort = this.getGroupSortField();
            if( newSortField == currentSort){
                newSortField.set('group_sort', newSortField.get('group_sort') > 0 ? -1 : 1);
            }else{
                if( currentSort) currentSort.unset('group_sort', {silent: true});
                newSortField.set('group_sort', -1);
            }
        }

    })
});
