define([
    'jquery',
    'underscore',
    'backbone',
    'animelist/Panel/AbstractPanelView',
    'animelist/FieldsSelect/FieldsCollection',
    'animelist/FieldsSelect/Fields/AbstractFieldView',
    'animelist/FieldsSelect/Fields/RangeSelectView',
    'animelist/FieldsSelect/Fields/MultiSelectView',
    'animelist/FieldsSelect/Fields/GenresSelectView',
    'animelist/FieldsSelect/Fields/DateRangeSelectView',
    'animelist/FieldsSelect/Fields/TextSelectView',
    'animelist/FieldsSelect/Fields/TypeAheadSelectView',
    'animelist/FieldsSelect/DefaultFields',
    'jquery-ui',
    'ion.rangeSlider'
], function($, _, Backbone, AbstractPanelView, FieldsCollection, AbstractFieldView, RangeSelectView,
            MultiSelectView, GenresSelectView, DateRangeSelectView, TextSelectView, TypeAheadSelectView){
    return AbstractPanelView.extend({

        initialize: function(options){
            AbstractPanelView.prototype.initialize.call(this, options);

            this.class = options.class;
            this.fieldsCollection = options.fieldsCollection;
            this.fieldsCollection.on('add', this.addField, this);
            this.fields_count = 1;
            this.fieldsCollection.on('reset', this.onReset, this);

            this.fieldsCollection.each(function(field){
                this.addField(field);
            }, this);
        },

        events: function(){
            return _.extend({},AbstractPanelView.prototype.events,{
            })
        },

        getFieldView: function(field){
            switch ( field.get('filter_type')){
                case 'range':
                    return RangeSelectView;
                case 'select':
                    return MultiSelectView;
                case 'genres':
                    return GenresSelectView;
                case 'daterange':
                    return DateRangeSelectView;
                case 'text':
                    return TextSelectView;
                case 'ahead':
                    return TypeAheadSelectView;
                default:
                    return AbstractFieldView;
            }
        },

        addDefaultFields: function(){
            this.fieldsCollection.addDefaultFields();
        },

        onReset: function(){
            this.$el.find('.panel-body').html('');
        },

        render: function(){
            this.$( ".panel-body").addClass("sortable");
            this.$( ".panel-body" ).sortable({
                placeholder: "ui-state-highlight",
                forcePlaceholderSize: true,
                tolerance: "pointer",
                start: $.proxy(this.onSortStart, this),
                stop: $.proxy(this.onSortStop, this),
                receive: $.proxy(this.onReceive, this),
                connectWith: ".sortable",
                delay: 200
            });

            return this;
        },

        addField: function(field, value, options){
            if (this.class == field.get('class')){
                this.showPanel();
                field.set('order', this.fields_count, {silent: true});
                this.fields_count++;
                var field_view_class = this.getFieldView(field);
                var field_view = new field_view_class({model: field});
                this.$el.find('.panel-body').append(field_view.el);
                field_view.render();
                if (this.class > 1){
                    this.hidePanel();
                }
            }
        },

        onSortStart: function(event, ui){
            this.startIndex = this.$(ui.item).index();
        },

        onReceive: function(event, ui){
            var moved_model = this.fieldsCollection.get(ui.item[0].dataset.cid);
            moved_model.set('class', this.class);
            this.onSortStop(event, ui);
        },

        onSortStop: function(event, ui){
            var moved_model = this.fieldsCollection.get(ui.item[0].dataset.cid);
            var previous_model_order, next_model_order;

            if (moved_model.get('class') == this.class) {

                var previous_item = $(ui.item).prev();
                if (previous_item.length) {
                    previous_model_order = this.fieldsCollection.get(previous_item[0].dataset.cid).get('order');
                } else {
                    previous_model_order = 0;
                }

                var next_item = $(ui.item).next();
                if (next_item.length) {
                    next_model_order = this.fieldsCollection.get(next_item[0].dataset.cid).get('order');
                } else {
                    next_model_order = this.fields_count;
                }

                var new_between_order = (previous_model_order + next_model_order)/2;
                moved_model.set('order', new_between_order);
                this.fieldsCollection.sort();
            }
        }
    })
});