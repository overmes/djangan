define([
    'jquery',
    'underscore',
    'backbone',
    'text!animelist/Pagination/PaginationTemplate.html'
], function($, _, Backbone, PaginationTemplate){
    return Backbone.View.extend({
        className: "pagination pagination-sm",
        tagName: "ul",

        events: {
            "click li": "onChangePage"
        },

        initialize: function(options){
            this.animeCollection = options.animeCollection;
            this.animeCollection.on('pagecountchange', this.render, this);
        },

        onChangePage: function(event){
            event.stopPropagation();
            event.preventDefault();
            var page = parseInt(event.currentTarget.dataset.page);
            this.animeCollection.setPage(page);
            this.render();
        },

        render: function(){
            var template = _.template(PaginationTemplate);
            this.$el.html(template({
                pageCount: this.animeCollection.pageCount,
                currentPage: this.animeCollection.currentPage
            }));
            this.setActivePage(this.animeCollection.currentPage);
            return this;
        },

        setActivePage: function(page){
            this.$el.find('.active').removeClass('active');
            this.$el.find('[data-page='+ page +']').addClass('active');
        }
    })
});
