define([
    'backbone',
    'text!animelist/Panel/AbstractPanelTemplate.html'
], function (Backbone, AbstractPanelTemplate) {
    return Backbone.View.extend({
        initialize: function (options) {
            var template = _.template(AbstractPanelTemplate);
            this.setElement( template({name:options.name}));
            this.bodyHidden = false;
        },
        events: {
            "click .panel-heading": 'onHideIconClick'
        },

        onHideIconClick: function(event){
            event.preventDefault();
            if( this.bodyHidden){
                this.showPanel();
            }else{
                this.hidePanel();
            }

        },


        showPanel: function(){
            this.$el.find('.content').show();
            this.$el.find('.panel-hide-icon').removeClass('glyphicon-chevron-down');
            this.$el.find('.panel-hide-icon').addClass('glyphicon-chevron-up');
            this.bodyHidden = false;
        },

        hidePanel: function(){
            this.$el.find('.content').hide();
            this.$el.find('.panel-hide-icon').removeClass('glyphicon-chevron-up');
            this.$el.find('.panel-hide-icon').addClass('glyphicon-chevron-down');
            this.bodyHidden = true;
        }

    });
});
