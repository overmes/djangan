define([
    'jquery',
    'underscore',
    'backbone'
], function($, _, Backbone){
    return Backbone.Model.extend({
        urlRoot: '/api/state',
        idAttribute: 'id',
        defaults: {
            error: ''
        },

        createUrl: function(){
            if( this.get('id')){

                return "http://{0}#state/{1}".format(window.location.host, this.get('id'));
            }else{
                return '';
            }
        },

        isNumber: function(n) {
          return !isNaN(parseFloat(n)) && isFinite(n);
        },

        normalizeId: function(){
            var split_id = this.get('id').split('-');
            if( split_id.length > 1 && this.isNumber(split_id[split_id.length - 1])){
                split_id.pop();
            }
            return split_id.join(" ");
        }
    })
});