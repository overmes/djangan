define([
    'jquery',
    'underscore',
    'backbone',
    'animelist/Panel/AbstractPanelView',
    'animelist/SaveState/SaveStateModel',
    'text!animelist/SaveState/SaveStateTemplate.html',
    'ladda',
    'spin'
], function ($, _, Backbone, AbstractPanelView, SaveStateModel, SaveTemplate, Ladda) {
    return AbstractPanelView.extend({
        initialize: function (options) {
            AbstractPanelView.prototype.initialize.call(this, options);
            this.fieldsCollection = options.fieldsCollection;
            this.animeCollection = options.animeCollection;
            this.userRequestModel = options.userRequestModel;
            this.settingsView = options.settingsView;

            this.stateModel = new SaveStateModel();
            this.stateModel.on('error', this.onError, this);
            this.stateModel.on('sync', this.onSync, this);

            this.hidePanel();

            this.setFieldsListeners();

        },

        events: function(){
            return _.extend({},AbstractPanelView.prototype.events,{
                "keypress form": "enterSubmit",
                "click #saveSubmit": "onSubmit",
                "click #stateUrl": "selectStateUrl"
            })
        },

        setFieldsListeners: function(){
            this.fieldsCollection.on('change', this.onFieldChange, this);
            this.fieldsCollection.on('add', this.onFieldChange, this);
            this.settingsView.settingsModel.on('change', this.onFieldChange, this);
        },

        stopFieldsListeners: function(){
            this.fieldsCollection.off('change', this.onFieldChange, this);
            this.fieldsCollection.off('add', this.onFieldChange, this);
            this.settingsView.settingsModel.off('change', this.onFieldChange, this);
        },

        onFieldChange: function(){
            if( this.stateModel.get('id')){
                this.stateModel.unset('id');
                this.$('#urlRow').hide();
                Backbone.history.navigate('main');
            }
        },

        setState: function(state){
            this.stateModel.clear();
            this.stateModel.set('id', state);
            this.stateModel.fetch();
        },

        applyState: function(){
            this.stopFieldsListeners();
            this.fieldsCollection.reset();
            var result = JSON.parse(this.stateModel.get('fields'));

            this.settingsView.setState(result['settings']);
            this.animeCollection.setPage(result['page'] || 1, {silent: true});
//            this.fieldsCollection.addDefaultFields();
            this.fieldsCollection.set(result['fields']);
            this.userRequestModel.set('username', result['username'] || '');
            this.stateModel.set('name', this.stateModel.normalizeId());

            this.setFieldsListeners();
        },

        onSync: function(){
            if( !this.stateModel.get('name')){
                this.applyState();
            }else{
                this.stateModel.set('url', this.stateModel.createUrl());
                Backbone.history.navigate('state/' + this.stateModel.get('id'));
            }
            this.render();
        },

        onError: function(model, resp, options){
            if( this.fieldsCollection.size() == 0){
                this.fieldsCollection.addDefaultFields();
            }
            var error = options && options.xhr ? "{0} {1}".format(options.xhr.status, options.xhr.statusText) : "No connection";
            this.stateModel.set('error', error);
            this.render();
        },

        onSubmit: function(e){

            var viewName = this.$('#viewName').val();
            if( viewName){
                this.stateModel.clear();
                this.stateModel.set({ name:viewName, fields: {fields: this.fieldsCollection,
                    username: this.userRequestModel.get('username'), page: this.animeCollection.currentPage,
                    settings: this.settingsView.settingsModel}});
                this.stateModel.save();
                var l = Ladda.create(this.$('button')[0]);
                l.start();
            }

        },

        render: function(){
            var template = _.template(SaveTemplate);
            this.$el.find('.content').html(template({state: this.stateModel.toJSON()}));
            return this;
        },

        enterSubmit: function (e) {
            var code = e.keyCode || e.which;
            if (code == 13) {
                e.preventDefault();
                this.onSubmit(e);
            }
        },

        selectStateUrl: function(){
            this.$("#stateUrl").select();
        }
    })
});