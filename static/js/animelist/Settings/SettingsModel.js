define([
    'backbone'
], function(Backbone){
    return Backbone.Model.extend({

        defaults: {
            groupView: false,
            sort_type: 'max',
            query_limit: 50,
            group_limit: 100,
            fluidView: false
        }
    })
});