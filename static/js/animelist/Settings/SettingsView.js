define([
    'backbone',
    'animelist/Panel/AbstractPanelView',
    'animelist/Settings/SettingsModel',
    'text!animelist/Settings/SettingsTemplate.html',
    'modelbinder',
    'ion.rangeSlider'
], function(Backbone, AbstractPanelView, SettingsModel, SettingsTemplate){
    return AbstractPanelView.extend({
        initialize: function(options){
            AbstractPanelView.prototype.initialize.call(this, options);
            this._modelBinder = new Backbone.ModelBinder();
            this.settingsModel = new SettingsModel();

        },

        events: function(){
            return _.extend({},AbstractPanelView.prototype.events,{
            })
        },

        render: function(){

            var template = _.template(SettingsTemplate);
            this.$el.find('.content').html(template({model: this.settingsModel}));
            this._modelBinder.bind(this.settingsModel, this.$el);
            this.initSlider();
            this.hidePanel();
            return this;
        },

        setState: function(settings){
            this.showPanel();
            this.settingsModel.set(settings);
            this.$("#queryLimit").ionRangeSlider("update", {from: this.settingsModel.get('query_limit')});
            this.$("#groupLimit").ionRangeSlider("update", {from: this.settingsModel.get('group_limit')});
            this.hidePanel();
        },

        initSlider: function(){
            var self = this;
            this.$("#queryLimit").ionRangeSlider({
                onFinish: function(slider){
                    self.settingsModel.set('query_limit', slider.fromNumber);
                }
            });
            this.$("#groupLimit").ionRangeSlider({
                onFinish: function(slider){
                    self.settingsModel.set('group_limit', slider.fromNumber);
                }
            });
        }
    })
});