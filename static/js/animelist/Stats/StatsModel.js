define([
    'backbone',
    'backbone_tastypie'
], function(Backbone){
    return Backbone.Model.extend({
        urlRoot: '/api/v1/animestats/',
        idAttribute: "_id",

        parse: function(response){
            for(var key in response){
                if(isNumber(response[key])){
                    response[key] = response[key] % 1 > 0 ? response[key].toFixed(2) : response[key];
                }
            }
            return response;
        },

        getStringId: function(){
            return decodeURIComponent(this.get('_id'));
        },

        defaults: {
        }
    })
});