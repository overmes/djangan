define([
    'backbone',
    'animelist/Panel/AbstractPanelView',
    'animelist/Stats/StatsModel',
    'text!animelist/Stats/StatsTemplate.html',
    'backbone_tastypie'
], function(Backbone, AbstractPanelView, StatsModel, StatsTemplate){
    return AbstractPanelView.extend({
        initialize: function(options){
            AbstractPanelView.prototype.initialize.call(this, options);
            this._modelBinder = new Backbone.ModelBinder();

            this.statsModel = new StatsModel();

            this.queryModel = options.queryModel;

            this.queryModel.on('change', this.queryModelChange, this);

            this.names = {
                "Results": "count",
                "Groups": "group_count",
                "Scores avg": "avg_score",
                "Weight avg": "avg_weight",
                "Episodes avg": "avg_episodes",
                "Episodes sum": "sum_episodes",
                "Duration avg": "avg_duration",
                "Duration sum": "sum_duration",
                "Scores count avg": "avg_scores",
                "Scores count sum": "sum_scores",
                "Members count avg": "avg_members",
                "Members count sum": "sum_members"
            }
        },

        events: function(){
            return _.extend({},AbstractPanelView.prototype.events,{
            })
        },

        queryModelChange: function(model){
            if (this.statsModel.getStringId() != JSON.stringify(model.get('filters')) || 'groupView' in model.changed) {
                this.statsModel.clear();
                this.statsModel.set('_id', encodeURIComponent(JSON.stringify(model.get('filters'))));
                this.statsModel.fetch({
                    data: model.getQuery()
                });
            }
        },

        render: function() {
            var template = _.template(StatsTemplate);
            this.$el.find('.content').html(template({names: this.names}));
            this._modelBinder.bind(this.statsModel, this.$el);
            this.hidePanel();
            return this;
        }
    })
});