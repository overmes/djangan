define([
    'jquery',
    'underscore',
    'backbone',
    'text!animelist/Table/SingleSortTemplate.html'
], function ($, _, Backbone, SingleSortTemplate) {
    return Backbone.View.extend({
        initialize: function (options) {
            this.fieldsCollection = options.fieldsCollection;
            this.settingsModel = options.settingsModel;

        },

        events: {
            'click .sortable': 'sortEvent'
        },

        sortEvent: function(event){
            var field_id = event.currentTarget.dataset.cid;
            var sort_type = event.currentTarget.dataset.type;
            var new_sort;
            if (sort_type == 'all'){
                new_sort = this.fieldsCollection.get(field_id);
                this.fieldsCollection.setSortField(new_sort);
            } else {
                new_sort = this.fieldsCollection.get(field_id);
                this.fieldsCollection.setGroupSortField(new_sort);
            }

        },

        render: function () {
            var template = _.template(SingleSortTemplate);
            this.setElement(template({columnsList: this.fieldsCollection.getSelectedModelsList()}));
            return this;
        }

    })
});