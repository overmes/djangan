define([
    'jquery',
    'underscore',
    'backbone',
    'text!animelist/Table/SingleTemplate.html',
    'text!animelist/Table/GroupTemplate.html',
    'text!animelist/Table/MediaTemplate.html'
], function ($, _, Backbone, SingleTemplate, GroupTemplate, MediaTemplate) {
    return Backbone.View.extend({
        initialize: function (options) {
            this.fieldsCollection = options.fieldsCollection;
            this.settingsModel = options.settingsModel;
            this.animeCollection = options.animeCollection;

            this.fieldsCollection.on('sort', this.render, this);
            this.animeCollection.on('reset', this.render, this);

            this.classesDict = this.getClasses();

        },

        events: {
            'click .sortable': 'sortEvent'
        },

        sortEvent: function(event){
            var field_id = event.currentTarget.dataset.cid;
            var sort_type = event.currentTarget.dataset.type;
            if (sort_type == 'all'){
                var new_sort = this.fieldsCollection.get(field_id);
                this.fieldsCollection.setSortField(new_sort);
            } else {
                var new_sort = this.fieldsCollection.get(field_id);
                this.fieldsCollection.setGroupSortField(new_sort);
            }

        },

        getClasses: function(){
            return {
                'image-image': "cell-image",
                'image-thumb': "cell-thumb",
                'title-anime_url': "cell-title"
            };
        },

        render: function () {
            var template;
            if (this.settingsModel.get('groupView')){
                template = _.template(GroupTemplate);
            } else {
//                template = _.template(MediaTemplate);
                template = _.template(SingleTemplate);
            }
            this.$el.html(template({
                animeCollection: this.animeCollection,
                columnsList: this.fieldsCollection.getSelectedModelsList(),
                classesDict: this.classesDict,
                settingsModel: this.settingsModel
            }));
            return this;
        }

    })
});