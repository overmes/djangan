define([
    'jquery',
    'underscore',
    'backbone',
    'moment'
], function ($, _, Backbone, moment) {
    return Backbone.Model.extend({
        idAttribute: "user_id",
        urlRoot: "/api/userscorerequest",
        defaults: {
            error: ''
        },

        parse: function(response){
            if( response.last_success_request ){
                response.last_success_request = moment(response.last_success_request).format('DD.MM.YY HH:mm');
            }
            return response;
        },

        getFields: function(){
            var user_id = this.get('user_id');
            var username = this.get('username');
            var sync_date = this.get('last_success_request');
            username = username.charAt(0).toUpperCase() + username.slice(1);
            var user_fields = [
                {
                    id: "userscore",
                    name: "user_scores."+ user_id + ".score",
                    label: username + " Score",
                    type: "user",
                    short: "Score",
                    format: "user_score",
                    date: sync_date,
                    filter_type: "range",
                    filter_opt: {min:0, max: 10, null: true},
                    'class': 1,
                    sortable: true
                },
                {
                    id: "usertype",
                    name: "user_scores."+ user_id + ".type",
                    label: username + " Type",
                    type: "user",
                    short: "Type",
                    format: "user_status",
                    date: sync_date,
                    filter_type: "select",
                    filter_opt: {values: [0,1,2,3,4,6]},
                    'class': 1,
                    sortable: true

                },
                {
                    id: "userdate",
                    name: "user_scores."+ user_id + ".date",
                    label: username + " Date",
                    type: "user",
                    short: "Date",
                    format: "date",
                    date: sync_date,
                    filter_type: "daterange",
                    'class': 1,
                    sortable: true
                }
            ];

            return user_fields;
        }
    })
});