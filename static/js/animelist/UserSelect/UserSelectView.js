define([
    'jquery',
    'underscore',
    'backbone',
    'animelist/Panel/AbstractPanelView',
    'animelist/UserSelect/UserSelectModel',
    'animelist/App/FieldModel',
    'text!animelist/UserSelect/UserSelectTemplate.html',
    'ladda',
    'spin'
], function ($, _, Backbone, AbstractPanelView, UserSelectModel, FieldModel, Template, Ladda) {
    return AbstractPanelView.extend({
        initialize: function(options){
            AbstractPanelView.prototype.initialize.call(this, options);
            this.fieldsCollection = options.fieldsCollection;
            this.userRequestModel = new UserSelectModel();

            this.userRequestModel.on('sync', this.onSync, this);
            this.userRequestModel.on('error', this.onError, this);
            this.userRequestModel.on('change:username', this.render, this);

            this.hidePanel();
        },

        events: function(){
            return _.extend({},AbstractPanelView.prototype.events,{
                "keypress form": "enterSubmit",
            "click #userSubmit": "onSubmit"
            })
        },

        render: function () {
            var template = _.template(Template);
            this.$el.find('.content').html(template({user_request: this.userRequestModel.toJSON()}));
            return this;
        },

        onError: function(model, resp, options){
            var error = options && options.xhr ? "{0} {1}".format(options.xhr.status, options.xhr.statusText) : "No connection";
            this.userRequestModel.set('error', error);
            this.render();
        },

        onSync: function(e){
            if( this.userRequestModel.get('last_success_request')){
                this.addUserFields();
            }
            this.render();
        },

        addUserFields: function () {
            var user_fields = this.userRequestModel.getFields();
            this.fieldsCollection.set(user_fields, {remove: false, sort: false});
        },

        onSubmit: function(e){

            var username = this.$el.find('#username').val();
            if( username != this.userRequestModel.get('username')){
                this.userRequestModel.clear({silent: true});
                this.userRequestModel.set({'username': username}, {silent: true});
            }

            if( username){
                var l = Ladda.create(this.$el.find('button')[0]);
                l.start();
                this.userRequestModel.save();
            }
        },

        enterSubmit: function (e) {
            var code = e.keyCode || e.which;
            if (code == 13) {
                e.preventDefault();
                this.onSubmit(e);
            }
        }
    })
});