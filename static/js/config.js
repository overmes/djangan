require.config({
    baseUrl: '/static/js/',
    paths: {
        ravenjs: 'libs/raven.min',
        underscore: 'libs/underscore-min',
        jquery: 'libs/jquery-2.1.0.min',
        backbone: 'libs/backbone-min',
        backbone_tastypie: 'libs/backbone-tastypie',
        backbone_paginator: 'libs/backgrid-paginator.min',
        text: 'libs/text',
        moment: 'libs/moment.min',
        'jquery-ui': 'libs/jquery-ui-1.10.3.custom.min',
        jquery_cookie: 'libs/jquery.cookie',
        ladda: 'libs/ladda.min',
        spin: 'libs/spin.min',
        'ion.rangeSlider': 'libs/ion.rangeSlider.min',
        'modelbinder': 'libs/Backbone.ModelBinder.min',
        'collectionbinder': 'libs/Backbone.CollectionBinder.min',
        bootstrap: 'libs/bootstrap.min',
        'bootstarpselect': 'libs/bootstrap-select.min',
        datepicker: 'libs/bootstrap-datepicker',
        helpers: 'libs/helpers',
        select2: 'libs/select2.min',
        jasmine: 'libs/jasmine',
        'jasmine-html': 'libs/jasmine-html'
    },

    shim: {
        jasmine: {
            exports: 'jasmine'
        },
        'jasmine-html':{
            deps: ['jasmine'],
            exports: 'jasmine'
        },
        select2: {
            deps: ['jquery']
        },
        ravenjs:{
            deps: ['jquery']
        },
        datepicker:{
            deps: ['jquery', 'bootstrap']
        },
        bootstrap: {
            deps: ['jquery']
        },
        'bootstrapselect':{
            deps: ['bootstrap']
        },
        'collectionbinder':{
            deps: ['backbone']
        },
        'modelbinder':{
            deps: ['backbone']
        },
        'ion.rangeSlider':{
            deps: ['jquery']
        },
        'jquery-ui':{
            deps: ['jquery']
        },
        'jquery_cookie':{
            deps: ['jquery']
        },
        'backbone_paginator':{
            deps: ['backbone']
        },
        'backbone_tastypie':{
            deps:['backbone']
        },
        'backbone': {
            deps: ['jquery', 'underscore']
        }
    }
});
