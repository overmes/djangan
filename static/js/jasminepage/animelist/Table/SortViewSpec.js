define([
    'animelist/Table/SortView',
    'jasminepage/libs/AllFieldsCollection',
    'animelist/Settings/SettingsModel'
], function (SortView, allFieldsCollection, SettingsModel) {
    var sortView;
    var settingsModel;
    beforeEach(function() {
        settingsModel = new SettingsModel();
        sortView = new SortView({fieldsCollection: allFieldsCollection, settingsModel: settingsModel});
    });

    describe("Sort View Spec", function () {
        it("Render proper html: " + allFieldsCollection.pluck('label'), function () {
            allFieldsCollection.each(function(field){
                field.set('selected', true);
                sortView.render();
                var currentFieldTh = sortView.$('[data-cid={0}]'.format(field.cid)).html();

                var re = new RegExp("{0}".format(field.get('short')));
                expect(currentFieldTh).toMatch(re);

                if (field.get('sortable')) {
                    expect(currentFieldTh).toMatch(/glyphicon/);
                } else {
                    expect(currentFieldTh).not.toMatch(/glyphicon/);
                }
            });

        });

        it("Render proper glyphicon: " + _.map(allFieldsCollection.where({sortable: true}), function(model){return model.get('label')}),
            function () {
                _.each(allFieldsCollection.where({sortable: true}), function(field){
                    var currentSort;
                    var click = false;
                    field.set('selected', true);
                    sortView.render();
                    allFieldsCollection.on('change:sort', function(){
                        click = true;
                    });

                    runs(function(){
                        currentSort = field.get('sort');
                        var currentField = sortView.$('[data-cid={0}]'.format(field.cid));
                        currentField.click();
                    });

                    waitsFor(function() {
                        return click;
                    }, "First Click must be occurred", 500);

                    runs(function(){
                        expect(field.get('sort')).toBe(currentSort == -1 ? 1:-1);
                    });

                    click = false;

                    runs(function(){
                        var currentField = sortView.$('[data-cid={0}]'.format(field.cid));
                        currentField.click();
                    });

                    waitsFor(function() {
                        return click;
                    }, "Second Click must be occurred", 500);

                    runs(function(){
                        expect(field.get('sort')).toBe(currentSort || 1);
                    });
                });
        });

    });
});