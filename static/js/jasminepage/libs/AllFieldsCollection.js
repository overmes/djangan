define([
    'animelist/FieldsSelect/FieldsCollection',
    'animelist/UserSelect/UserSelectModel'
], function (FieldsCollection, UserSelectModel) {
    var fieldsCollection = new FieldsCollection();
    fieldsCollection.addDefaultFields();

    var userSelectModel = new UserSelectModel({
        username: 'overmes',
        user_id: '1',
        last_success_request: moment().format('DD.MM.YY HH:mm')
    });

    fieldsCollection.set(userSelectModel.getFields(), {remove: false, sort: false});

    return fieldsCollection;
});